# Routes

Each route should have its own folder and at a minimum expose at least
one routing method.

Example:
```javascript
  var _ = require('lodash');
  var express = require('express');
  var router = express.Router();
  var memberService = require('../../services/member.service');

  router.get('/', function(req, res, next) {
    res.status(200).end();
  });
```