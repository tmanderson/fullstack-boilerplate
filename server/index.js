'use strict';

var _ = require('lodash');
var B = require('bluebird');
var express = require('express');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var passport = require('passport');

var mongoose = require('mongoose');
var config = require('./config');
var auth = require('./config/auth');

var app = express();

mongoose.connect(config.mongo);

app.use(express.static('client'));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(session(config.session));
app.use(passport.initialize());
app.use(passport.session());

app.get('/activate/:email', auth.activate);
app.post('/login', auth.login);
app.use('/api', auth.verify, require('./routes'));

app.all('/*', function(req, res) {
  res.status(200).sendFile('index.html', { root: 'client' });
});

if(config.env === 'local') {
  // do any dev data scaffolding here
}

app.listen(config.port, function() {
  console.log('Listmas listening on port %s', config.port);
});