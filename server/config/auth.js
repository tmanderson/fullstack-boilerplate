'use strict';

var _ = require('lodash');
var B = require('bluebird');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var Member = require('../models/member.model');

var generatePassword = require('password-generator');
var emailService = require('../services/email.service');

passport.use(new LocalStrategy({
    usernameField: 'name'
  },
  function(username, password, done) {
    Member.findOne({
      $or: [
        { name: _.capitalize(username) },
        { email: username }
      ]
    }, function(err, user) {
      if(err) return done(err);

      if(!user) return done(null, false, { message: 'Incorrect username.' });
      if(!user.validPassword(password)) return done(null, false, { message: 'Incorrect password.' });

      return done(null, user);
    });
  }
));

passport.serializeUser(function(user, done) {
  if(!user) done('No user session found', null);
  done(null, user._id);
});

passport.deserializeUser(function(id, done) {
  Member.findById(id, function(err, user) {
    done(err, user);
  });
});

_.extend(module.exports, {
  activate: function(req, res, next) {
    Member.find({ email: req.query.email }).exec()
      .then(function(member) {
        var pass = generatePassword();

        Member.update({ _id: member._id }, {
          activated: true,
          password: pass
        })
        .then(function() {
          emailService.activate(member, pass);
          req.login(member, function(err) {
            if(!err) res.redirect('/');
            res.status(500).end();
          });
        });
      });
  },

  login: function(req, res, next) {
    passport.authenticate('local')(req, res, function() {
      if(req.user) return res.redirect('/api/me');
      res.status(403).json({ message: 'invalid session' });
    });
  },

  verify: function(req, res, next) {
    if(req.user) return next();
    res.status(403).json({ message: 'invalid session' });
  }
});