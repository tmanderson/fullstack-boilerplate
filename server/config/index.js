'use strict';

var _ = require('lodash');

_.extend(module.exports, {
  env: process.env.ENV || 'local',
  mongo: process.env.MONGOLAB_URI || 'mongodb://localhost/listmastime',
  port : process.env.PORT || 2000,

  domain: process.env.DOMAIN || 'https://listmastime.herokuapp.com',

  session: {
    name: 'listmastime.session',
    secret: process.env.SESSION_SECRET || 'b$Z3d4CQbV+qdLDaPcn',

    cookie: {
      path: '/',
      httpOnly: false,
      secure: false,
      maxAge: null
    },
    resave: true,
    saveUninitialized: true
  }

});
