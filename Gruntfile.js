module.exports = function(grunt) {
  'use strict';

  require('load-grunt-tasks')(grunt);

  grunt.initConfig({
    config: {
      styles: [ 'client/assets/styles/**/*.scss', 'client/src/**/*.scss' ],
      client: [ 'client/src/main.js', 'client/src/**/*.js' ],
      server: [ 'server/**/*.js' ]
    },

    watch: {
      src: {
        files: '<%= config.js %>',
        tasks: [ 'jshint', 'injector' ],
        options: {
          spawn: true,
          livereload: 35730
        }
      },

      scss: {
        files: '<%= config.styles %>',
        tasks: [ 'concat:sass', 'sass' ]
      }
    },

    connect: {
      server: {
        options: {
          port: 4000,
          hostname: 'localhost'
        }
      }
    },

    concat: {
      sass: {
        files: {
          'client/assets/styles/main.scss': [ '<%= config.styles %>' ]
        }
      }
    },

    sass: {
      options: {
        sourceMap: true
      },
      dist: {
        files: {
          'client/assets/styles/main.css': 'client/assets/styles/main.scss'
        }
      }
    },

    clean: {
      sass: ['client/assets/styles/main.scss']
    },

    jshint: {
      options: {
        jshintrc: true
      },

      all: [ '<%= config.client %>', '<%= config.server %>' ]
    },

    injector: {
      options: {
        addRootSlash: false,
        ignorePath: 'client'
      },

      app: {
        files: {
          'client/index.html': [ 'bower.json', '<%= config.client %>', 'client/assets/styles/main.css' ]
        }
      }
    },
  });

  grunt.registerTask('default', [ 'clean:sass', 'concat:sass', 'sass', 'jshint', 'injector' ]);
  grunt.registerTask('serve', [ 'connect', 'watch' ]);
};